import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme"
import { add_Task, change_theme, delete_task, done_task, edit_task, LL_task, update_task } from "../types/ToDoListTypes"
import { arrThemes } from "../../Themes/ThemeManager";

const initialState = {
    themeToDoList: ToDoListDarkTheme,
    taskList: [
      {id:'task-1', taskName: 'task 1', done:false, },
      {id:'task-2', taskName: 'task 2', done:true, },
      {id:'task-3', taskName: 'task 3', done:false, },
      {id:'task-4', taskName: 'task 4', done:true, },
    ],
    taskEdit: {id:'task-0', taskName: 'task 0', done:false, },
}

export let ToDoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case add_Task: {
      // Kiểm tra rỗng
      if(action.newTask.taskName.trim() === ""){
        alert('Chưa nhập dữ liệu vào TaskName!!!');
        return{...state}
      }
      // Kiểm tra tồn tại
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(task => task.taskName === action.newTask.taskName);
      if(index !== -1){
        alert('TaskName đã tồn tại!!!');
      }
      taskListUpdate.push(action.newTask);

      state.taskList = taskListUpdate; 
      return {...state}
    }
    case change_theme: {
      let theme = arrThemes.find(theme => theme.id == action.themeId);
      if(theme){
        state.themeToDoList = {...theme.theme};
      }
      return {...state}
    }
    case done_task:{
      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex(task => task.id == action.taskId);
      if(index !== -1){
        taskListUpdate[index].done = true;
      }

      state.taskList = taskListUpdate;
      return {...state}
    }
    case delete_task:{
      let taskListUpdate = [...state.taskList];

      taskListUpdate = taskListUpdate.filter(task => task.id !== action.taskId);

      state.taskList = taskListUpdate;
      return {...state}
    }
    case LL_task:{
      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex(task => task.id == action.taskId);
      if(index !== -1){
        taskListUpdate[index].done = false;
      }

      state.taskList = taskListUpdate;
      return {...state}
    }
    case edit_task:{
      
      return {...state, taskEdit:action.task}
    }
    case update_task:{
      state.taskEdit = {...state.taskEdit, taskName:action.taskName};

      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex(task => task.id === state.taskEdit.id);

      if(index !== -1){
        taskListUpdate[index] =  state.taskEdit;
      }

      state.taskList = taskListUpdate;

      return {...state}
    }

  default:
    return {...state}
  }
} 


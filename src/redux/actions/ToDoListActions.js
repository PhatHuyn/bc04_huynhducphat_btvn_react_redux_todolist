import { add_Task, change_theme, delete_task, done_task, edit_task, LL_task, update_task } from "../types/ToDoListTypes";

export const addTaskAction = (newTask) => ({
  type: add_Task,
  newTask
})

export const ChangeThemeAction = (themeId) => ({
    type: change_theme,
    themeId
})

export const DoneTaskAction = (taskId) => ({
    type: done_task,
    taskId
})

export const TroLaiTaskAction = (taskId) => ({
    type: LL_task,
    taskId
})

export const DeletaTaskAction = (taskId) => ({
    type: delete_task,
    taskId
})

export const EditTaskAction = (task) => ({
    type: edit_task,
    task
})

export const UpdateTaskAction = (taskName) => ({
    type: update_task,
    taskName
})
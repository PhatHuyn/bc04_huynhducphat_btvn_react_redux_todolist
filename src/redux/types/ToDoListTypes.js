export const add_Task = 'add_Task';
export const change_theme = 'change_theme';
export const done_task = 'done_task';
export const LL_task = 'LL_task';
export const delete_task = 'delete_task';
export const edit_task = 'edit_task';
export const update_task = 'update_task';

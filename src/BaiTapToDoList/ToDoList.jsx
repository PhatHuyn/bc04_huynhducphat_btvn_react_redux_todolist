import React, { Component } from "react";
import { Container } from "../ComponentsToDoList/Container";
import { ThemeProvider } from "styled-components";
import { Dropdown } from "../ComponentsToDoList/Dropdown";
import { Heading3 } from "../ComponentsToDoList/Heading";
import { Label, Input, TextField } from "../ComponentsToDoList/TextField";
import { Button } from "../ComponentsToDoList/Button";
import { Table, Thead, Tbody, Th, Tr, Td } from "../ComponentsToDoList/Table";
import { connect } from "react-redux";
import {
  addTaskAction,
  ChangeThemeAction,
  DeletaTaskAction,
  DoneTaskAction,
  EditTaskAction,
  TroLaiTaskAction,
  UpdateTaskAction,
} from "../redux/actions/ToDoListActions";
import { arrThemes } from "../Themes/ThemeManager";

class ToDoList extends Component {
  state = {
    taskName: "",
  };

  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ textAlign: "left" }}>{task.taskName}</Th>
            <Th style={{ textAlign: "right" }}>
              <Button
                onClick={() => {
                  this.props.dispatch(EditTaskAction(task));
                }}
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(DoneTaskAction(task.id));
                }}
                style={{ marginLeft: "10px", marginRight: "10px" }}
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(DeletaTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ textAlign: "left" }}>{task.taskName}</Th>
            <Th style={{ textAlign: "right" }}>
              <Button
                onClick={() => {
                  this.props.dispatch(TroLaiTaskAction(task.id));
                }}
                style={{ marginLeft: "10px", marginRight: "10px" }}
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(DeletaTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTheme = () => {
    return arrThemes.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  // Xử lý với React phiển bản 16.0 nhận vào props mới được thực thi trước render
  // componentWillReceiveProps(newProps) {
  //   console.log("this.props", this.props);
  //   console.log("new props", newProps);
  //   console.log("là gì", newProps.taskEdit);
  //   this.setState({
  //     taskName: newProps.taskEdit.taskName,
  //   });
  // }

  // static getDerivedStateFromProps(newProps, currentState) {
  //   let newState = { ...currentState, taskName: newProps.taskEdit.taskName };
  //   return newState;
  // }

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.dispatch(ChangeThemeAction(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3 style={{ textAlign: "left" }}>To Do List</Heading3>
          <div style={{ textAlign: "left" }}>
            <TextField
              value={this.state.taskName}
              onChange={(e) => {
                this.setState({
                  taskName: e.target.value,
                });
              }}
              name="taskName"
              className="w-50"
              label="Task name"
            />
            <Button
              onClick={() => {
                let { taskName } = this.state;
                let newTask = {
                  id: Date.now(),
                  taskName: taskName,
                  done: false,
                };
                this.props.dispatch(addTaskAction(newTask));
              }}
              style={{ marginLeft: "60px", paddingBottom: "2px" }}
            >
              <i className="fa fa-plus"></i> Add Task
            </Button>
            <Button
              onClick={() => {
                this.props.dispatch(UpdateTaskAction(this.state.taskName));
              }}
              style={{ marginLeft: "10px", paddingBottom: "2px" }}
            >
              <i class="fa fa-upload"></i> Update Task
            </Button>
          </div>
          <hr />
          <Heading3 style={{ textAlign: "left" }}>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3 style={{ textAlign: "left" }}>Task completed</Heading3>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
